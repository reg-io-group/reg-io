reg.io
==
### Ein soziales Netzwek mit Fokus auf das Lokale.

## Was machen wir? (und warum?)

### Das Problem
* Echokammern haben isolierende Wirkung. Diese lösen sich durch unsere
fixen Regiokanäle auf.
* Es gibt Trennungen zwischen Mensch und Politik, zwischen Regionalität
und Globalität und sogar zwischen den Menschen selbst.
* Das Mediums Internet als Kommunikationsplattform wird in seiner
aktuellen Form primär zur Selbstdarstellung genutzt.
* Die direkte Nutzung als Werkzeug in der Kommuniaktion vor Ort fristet
ein Nischendasein in vielen einzelnen Applikationen ohne Schnittstelle
untereinander.

### Die Lösung
Ein online Chat-System in dem jedes Dorf, jede Stadt, jede Region, jeder
Kreis, jedes Land als Konversationskanal hierarchisch repräsentiert wird.  
Hier kann man regional netzwerken, lokal partizipieren, Chatten mit
Mitbürgern, andere Regionen besuchen, sich zuhause fühlen.  
Im Kanal wird über lokale Themen besprochen. Im Kanal einer kleinen
Region finden andere Gespräche statt, als z.B. in dem übergeordneten
Länderkanal!  
Die Kanäle sind außerdem mit Chatbots ausgestattet, über welche offene
Daten aus Politik und Verwaltung (OK-Labs, Kollaborieren? Kai?),
regionale Zeitungen oder das Wetter abgefragt werden können.  

## Wer sind wir?
Mathias und Victor sind zwei Studierende der Hochschule für Bildende
Künste Saar.  
Das Projekt wird außerdem assoziiert mit dem Projekt s.coop, welches im
Rahmen der Initiative "Innovation Hubs@Campus" vom Stifterverband Saar
gefördert wird.

## Was brauchen wir?
* Ethische Bedenken und Unterstützungsideen bitte per Mail an
mail@giers.io.
* Für das Thema Sicherheit benötigen wir noch Hilfe im Bereich der
Cybersecurity.
* ...es stehen viele potentielle Probleme in Aussicht!
  
Wir freuen uns über jeden Kontakt, jede Frage, jede Idee!

## Wie kann man mitmachen?
Unser Leitfaden für Unterstützer / Verhaltenskodex / Roadmap ist noch in
Arbeit.  
Kontaktiert uns einfach!

## Kontakt
Bei Problemen mit der Software bitte ein Issue formulieren.  
Für alles andere: mail@giers.io

