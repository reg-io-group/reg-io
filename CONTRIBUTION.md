### Vielen Dank für das Interesse daran reg.io zu unterstützen!

Wie kann man uns helfen?  
- **In Kontakt treten!** Gute Fragen, Anregungen, Ideen und konstruktive Kritik sowie das aufzeigen uns unbekannte Bugs sind große Hilfe. Wir sind offen und freuen uns über jeden Beitrag!
Hierfür bitte diese eMail-Adresse nutzen oder auf GitLab ein Issue eröffnen.
- **Programmierung:** Zur Pflege der Plattform und zur steten Weiterentwicklung ist Hilfe jederzeit Willkommen. Wir versuchen den Code so lesbar wie möglich zu gestalten und halten uns an gängige Richtlinen der Kommentarformatierung.
- **Moderation:** wir möchten eine offene aber angenehme Atmosphäre auf unserer Plattform. Sollte sich ein Nutzer an einem Beitrag stören, gibt es die Möglichkeit diesen zu melden. Bitte nutzt diese Möglichkeit!
Sollte Interesse daran bestehen unserem Modeatorenteam zuzustoßen kann man sich an unsere Bewerbungshomepage wenden.
- **Nutzer:** Eine Plattform lebt von der Teilhabe. Die beste / wichtigste und einfachste Form uns zu unterstützen ist also mitzumachen und andere dazu zu animieren dies auch zu tun.
